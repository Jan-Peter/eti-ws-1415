\documentclass[10pt,landscape]{scrartcl}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% imports
\usepackage{multicol}
\usepackage{calc}
\usepackage{ifthen}
\usepackage[landscape]{geometry}
\usepackage{amsmath,amsthm,amsfonts,amssymb}
\usepackage{hyperref}
\usepackage[utf8x]{inputenc} % utf8x for unix, ansinew for windows
\usepackage[ngerman]{babel}
\usepackage{mathtools}
\usepackage{latexsym}
\usepackage[T1]{fontenc}
\usepackage{cite}
\usepackage[pdftex]{graphicx,color,overpic}
\usepackage{subcaption}
\usepackage{fancyhdr}
\usepackage{tabularx}
\usepackage{ctable}
\usepackage{array}
\usepackage{lscape} % landscape mode
\usepackage{fancyvrb} % fancy verbatim
\usepackage[german]{varioref}
\usepackage{paralist}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\pdfinfo{
  /Title (cheat_sheet.pdf)
  /Creator (TeX)
  /Producer (pdfTeX 1.40.0)
  /Author (Jan-Peter Hohloch)
  /Subject (Cheat sheet)
  /Keywords (eti, ETI, formelsammlung, cheatSheet)}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% cheat sheet specifics

% This sets page margins to .5 inch if using letter paper, and to 1cm
% if using A4 paper. (This probably isn't strictly necessary.)
% If using another size paper, use default 1cm margins.
\ifthenelse{\lengthtest { \paperwidth = 11in}}
    { \geometry{top=.5in,left=.5in,right=.5in,bottom=.5in} }
    {\ifthenelse{ \lengthtest{ \paperwidth = 297mm}}
        {\geometry{top=1cm,left=1cm,right=1cm,bottom=1cm} }
        {\geometry{top=1cm,left=1cm,right=1cm,bottom=1cm} }
    }

% Turn off header and footer
\pagestyle{empty}

% Redefine section commands to use less space
\makeatletter
\renewcommand{\section}{\@startsection{section}{1}{0mm}%
                                {-1ex plus -.5ex minus -.2ex}%
                                {0.5ex plus .2ex}%x
                                {\normalfont\large\bfseries}}
\renewcommand{\subsection}{\@startsection{subsection}{2}{0mm}%
                                {-1explus -.5ex minus -.2ex}%
                                {0.5ex plus .2ex}%
                                {\normalfont\normalsize\bfseries}}
\renewcommand{\subsubsection}{\@startsection{subsubsection}{3}{0mm}%
                                {-1ex plus -.5ex minus -.2ex}%
                                {1ex plus .2ex}%
                                {\normalfont\small\bfseries}}
\makeatother

% Define BibTeX command
\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
    T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}

% Don't print section numbers
\setcounter{secnumdepth}{0}


\setlength{\parindent}{0pt}
\setlength{\parskip}{0pt plus 0.5ex}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Some custom commands to make life easier
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\du}{\hspace{0pt}} % dummy command
\newcommand{\term}[1]{\textbf{#1}}

% general math commands
\renewcommand{\:}{\ensuremath{\;\;}}
\newcommand{\ds}{\displaystyle}
\newcommand{\lp}{\ensuremath{\left}}
\newcommand{\rp}{\ensuremath{\right}}
\newcommand{\abs}[1]{\ensuremath{\lp|#1\rp|}}
\newcommand{\limv}[1]{\ensuremath{\lim\limits_{#1 \to \infty}}}
\newcommand{\limes}[1]{\ensuremath{\lim\limits_{#1}}}

% commands for various types of sets
\newcommand{\set}[1]{\lp\{#1\rp\}}
\newcommand{\rationaln}{\ensuremath{\mathbb{Q}}}
\newcommand{\realn}{\ensuremath{\mathbb{R}}}
\newcommand{\complexn}{\ensuremath{\mathbb{C}}}
\newcommand{\naturaln}{\ensuremath{\mathbb{N}}}
\newcommand{\naturalnz}{\ensuremath{\mathbb{N}_0}}
\newcommand{\integers}{\ensuremath{\mathbb{Z}}}

% relations (with text, smashed operators)
\renewcommand{\implies}[1][]{\ensuremath{\overset{\overset{#1}{}}{\Rightarrow}}}
\newcommand{\impliest}[1][]{\ensuremath{\overset{\overset{\text{#1}}{}}{
\Rightarrow}}}
\newcommand{\eq}[1][]{\ensuremath{\overset{#1}{=}}}
\newcommand{\eqt}[1]{\ensuremath{\overset{\text{#1}}{=}}}
\newcommand{\leqt}[1]{\ensuremath{\overset{\text{#1}{\leq}}}}
\newcommand{\geqt}[1]{\ensuremath{\overset{\text{#1}{\geq}}}}
\newcommand{\gt}{\ensuremath{>}}
\newcommand{\lt}{\ensuremath{<}}
\newcommand{\smleqt}[1]{\ensuremath{\smashoperator{\mathop{\leq}^{\text{#1}}}}}
\newcommand{\smeq}[1]{\ensuremath{\smashoperator{\mathop{=}^{#1}}}}
\newcommand{\smeqt}[1]{\ensuremath{\smashoperator{\mathop{=}^{\text{#1}}}}}
\newcommand{\smequivt}[1]{\ensuremath{\smashoperator{\mathop{\equiv}^{\text{#1}}
}}}
\newcommand{\smgeqt}[1]{\ensuremath{\smashoperator{\mathop{\geq}^{\text{#1}}}}}
\newcommand{\smgt}[1]{\ensuremath{\smashoperator{\mathop{>}^{\text{#1}}}}}
\newcommand{\smlt}[1]{\ensuremath{\smashoperator{\mathop{<}^{\text{#1}}}}}
\newcommand{\smimpt}[1]{\ensuremath{\smashoperator{\mathop{\implies}^{\text{#1}}
}}}

% proper table rule
\newcommand{\otoprule}{\midrule[\heavyrulewidth]}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% custom environments

% enumeration with smaller skips
\newenvironment{tightenum}[1][1]
    {\begin{enumerate}[#1]
    \setlength{\itemsep}{2pt}
    \setlength{\parskip}{0pt}
    \setlength{\parsep}{0pt}}
    {\end{enumerate}}

% itemize with smaller skips
\newenvironment{tightitemize}
    {\begin{itemize}
    \setlength{\itemsep}{2pt}
    \setlength{\parskip}{0pt}
    \setlength{\parsep}{0pt}}
    {\end{itemize}}

\newenvironment{study}[2]
  {
    \small{\textbf{#1}} \footnotesize{(#2)}\\
  }
  {
    \vspace{-0.3cm}
    \begin{center} \rule{\linewidth}{0.25pt} \end{center}
  }

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}
\raggedright
\footnotesize

% use multicols* to avoid auto balancing of columns
\begin{multicols*}{3}

% multicol parameters
% These lengths are set only within the two main columns
%\setlength{\columnseprule}{0.25pt}
\setlength{\premulticols}{1pt}
\setlength{\postmulticols}{1pt}
\setlength{\multicolsep}{1pt}
\setlength{\columnsep}{2pt}

\begin{center}
     \Large{\textbf{ETI}} \\
\end{center}
\section{Boolsche Algebra}
\subsection{Huntingtonsche Axiome}
\begin{tightenum}[H1:]
    \item Kommutativität
    \item Distributivität
    \item $a\vee 0=a$, $a\wedge 1=a$
    \item $a\vee \overline{a} = 1$, $a\wedge\overline{a}$
\end{tightenum}
\begin{tightenum}[a)]
    \item Assoziativität
    \item Idempotenz ($a\wedge a=a$)
    \item Absorption ($a\vee (a\wedge b)=a$)
    \item DeMorgan
\end{tightenum}
\subsection{Normalformen}
    \begin{tightitemize}
        \item Minterm $\Rightarrow 1$
        \item Maxterm $\Rightarrow 0$
        \item Min-/Maxterm enthält alle Variablen $\rightarrow$ Normalform
        \item Minimalformen: möglichst viele Variablen sparen
        \item DMF: 1-Blöcke
        \item KMF: 0-Blöcke und negieren (DeMorgan $\rightarrow \vee$)
        \item Quine-McCluskey (zwei Tafeln, erst Primimplikanten finden, dann auswählen)
    \end{tightitemize}
\section{Schaltungsentwurf}
    \subsection{Schaltzeichen}\textit{siehe Skript, Foliensatz}
    \subsection{Addierer}
        \begin{tightitemize}
            \item Halbaddierer bildet aus A und B Summe(S) und Übertrag(C)
            \item Volladdierer bildet aus $A_i$, $B_i$ und $C_{i-1}$ $S_i$ und $C_i$
        \end{tightitemize}
\section{Hazards}
    \begin{tightitemize}
        \item Totzeitmodell: Verzögerungen werden nach vorne durchgeschoben
        \item Hazardfehler $\Rightarrow$ Hazard, nicht umgekehrt!
        \item Hazards: statis 0/1 oder dynamisch 01/10
        \item Funktionshazard: Hazardbehaftete Funktion, jedes Schaltnetz betroffen
        \item Strukturhazard: durch Umbau des Schaltnetzes zu beseitigen
        \item Erkennen: alle Wege im KV-Diagramm überprüfen, bei Wechsel: Funktionshazard (Eingangsvariablen) bzw. Strukturhazard (Pfadvariablen)
        \item Beseitigung 1-Strukturhazard: Zusätzliches UND-Glied eines Primimplikanten im KV-Diagramm
        \item Beseitigung 0-Strukturhazard: Zusätzliches ODER-Glied eines Primimplikates im KV-Diagramm
    \end{tightitemize}
    \subsection{Satz von Eichelberger}
        Ein Schaltnetz, das alle Primimplikanten disjunktiv oder alle Primimplikate konjunktiv realisiert, ist frei von statischen Strukturhazards. Außerdem frei von dynamischen Strukturhazards, wenn sich nur eine Eingangsvariable ändert.
\section{Speicherlieder}
RS realisiert über NOR, immer Rückkopplung nötig
    \begin{tightitemize}
        \item[RS:] Set/Reset 00 speichern, 11 ungültig
        \item[JK:] Jump/Kill 00 speichern, 11 invertieren
        \item[D:] nur ein Eingang, speichert aktuelle Eingabe
        \item[T:] kein Eingang (nur Takt), wechselt Zustand jeden Takt
    \end{tightitemize}
    \begin{tightitemize}
        \item{Master-Slave:} Slave wird im Gegentakt gesteuert
        \item{Taktflankensteuerung:} Sicherer gegen Störimpulse
    \end{tightitemize}
\section{Schaltwerksanalyse}
    \begin{tightitemize}
        \item[Mealy:] Zustand + Eingabe bestimmen Ausgabe
        \item[Moore:] Ausgabe durch Zustand determiniert
        \item[Medwedev:] Moore +  Ausgabe ist neuer Zustand
    \end{tightitemize}
\section{Elektrotechnische Grundlagen}
\subsection{Zusammenhänge}
    \begin{tightitemize}
        \item $1C=1As$
        \item $\vec{F}=\vec{E}q$
        \item $W=\vec{F}\cdot\vec{r}$
        \item $U_{1,2}=\int_{r_1}^{r_2} \vec{E}d\vec{r}$, $E=\frac{U}{d}$
        \item $W_{1,2}=q\cdot U_{1,2}$
        \item $D=\frac{Q}{A}=\varepsilon_0\vec{E}$ (el. Flussdichte)
        \item $U=RI$
        \item $W=I\cdot t \cdot U=I^2\cdot R\cdot t$
        \item $P=UI=\frac{W}{t}=I^2\cdot R$
        \item $Q=C\cdot U$
    \end{tightitemize}
\subsection{Konstanten}
\begin{tightitemize}
        \item $e_0=1.602 \cdot 10^{-19}C$ (Elementarladung)
        \item $\varepsilon_0=8.854\cdot 10^{-12}\frac{C^2}{Nm^2}$
        \item $\mu_0=1.2566\cdot 10^{-6} \frac{Vs}{Am}$
    \end{tightitemize}
\subsection{Formeln}
    \begin{tightitemize}
        \item $\vec{F}=\frac{1}{4\pi\varepsilon_0}\frac{Q_1\cdot Q_2}{r^2}\cdot \vec{r_0}$
        \item $\vec{E}=\frac{1}{4\pi\varepsilon_0}\frac{Q}{r^2}\vec{r_0}$
        \item $D=\frac{1}{4\pi}\frac{Q}{r^2}$
        \item $U=\frac{Q}{\varepsilon_0\varepsilon_rA}\cdot d$
        \item $C=\varepsilon_0\varepsilon_r\cdot \frac{A}{d}$
        \item $R=\varrho\cdot \frac{l}{A}$ (Widerstand im Kabel)
        \item $\vec{F}=\mu_0\mu_r I\vec{s}\times\vec{H}$
        \item $\vec{B}=\mu_0\mu_r\vec{H}$
        \item $U_{H}=-R_H\cdot\frac{IB}{d}$
        \item $L=\mu_0\mu_r\frac{N^2\cdot A}{l}$
    \end{tightitemize}
\subsection{Kirchoff'sche Gesetze}
\subsubsection{Knotenregel (1. Gesetz)}
    In einem Knoten ist die Summe aller Ströme 0.
\subsubsection{Maschenregel (2. Gesetz)}
    In einer Masche ist die Summe der Spannungen (Umlaufspannung) 0.
\subsection{Schaltungsformen}
    \begin{tightitemize}
        \item[Parallel:] $U=konst,\ I=\sum_i I_i,\ \frac{1}{R}=\sum_i\frac{1}{R_i}$
        \item[Reihe:] $U=\sum_i U_i,\ I=konst,\ R=\sum_i R_i$
    \end{tightitemize}
\subsection{Messen}
    \begin{tightitemize}
        \item[Stromstärke:] in Reihe, kleiner Innenwiderstand
        \item[Spannung:] parallel, großer Innenwiderstand
        \item Messung von beiden gleichzeitig bedingt Fehler in einem Wert
    \end{tightitemize}
\section{Netzwerkanalyse}
\begin{tightitemize}
    \item Knoten: Summe der Ströme ist 0
    \item Maschen: Baum aufspannen, dann einzeln Kanten hinzunehmen und so Maschen schließen. Umlaufspannung ist 0.
    \item Mit Ohm'schem Gesetz $U= RI$, $U_i$ durch $I_i$ ersetzen und Gleichungssystem lösen. \textit{($U_0$ steht dann rechts des $=$.)}
\end{tightitemize}
\subsection{Maschenanalyse}
    \begin{tightitemize}
        \item Baum aufspannen, dann einzeln Kanten hinzunehmen und so Maschen schließen. Umlaufspannung ist 0.
        \item Mit Ohm'schem Gesetz $U= RI$, $U_i$ durch $I_i$ ersetzen und Gleichungssystem lösen. \textit{($U_0$ steht dann rechts des $=$.)}
    \end{tightitemize}
\subsection{Elekto-Magnetisches Feld}
    \begin{tightitemize}
        \item Rechte-Hand-Regel: Daumen in Stromrichtung, Finger in Feldlinienrichtung ($\rightarrow$ Gegenuhrzeigersinn)
        \item Drei-Finger-Regel: Daumen - Strom, Zeigefinger - Magnetfeldrichtung, Mittelfinger - Lorentzkraft, Hand: pos. rechts, neg. links
        \item[Hystereseschleife:] Kurve, die das Verhalten der magnetischen Flussdichte bei zeitlich veränderlicher Feldstärke anzeigt.
    \end{tightitemize}
\subsection{Schaltvorgänge}
    \subsubsection{Widerstand}
        sprunghafte Änderung
    \subsubsection{Kondensator}
        \begin{tightitemize}
            \item[Einschalten:]
            \item $i=\frac{U_0}{R}\cdot e^{-\frac{t}{RC}}$
            \item $u_c=U_0\cdot \left(1-e^{-\frac{t}{RC}}\right)$
            \item[$\Rightarrow$] Strom steigt sprunghaft und fällt dann, Spannung steigt
            \item[Ausschalten:]
            \item $u_c=U_0\cdot e^{-\frac{t}{RC}}$
            \item $i=-\frac{U_0}{R}\cdot e^{-\frac{t}{RC}}$
            \item[$\Rightarrow$] Spannung nimmt ab, Strom wird umgekehrt und nimmt dann (betragsmäßig) ab
        \end{tightitemize}
    \subsubsection{Spule}
        \begin{tightitemize}
            \item[Einschalten:]
            \item $u_L=U_0\cdot e^{-\frac{R}{L}t}$
            \item $i=\frac{U_0}{R}\cdot \left(1-e^{-\frac{R}{L}t}\right)$
            \item[$\Rightarrow$] Spannung steigt sprunghaft und fällt dann, Strom steigt
            \item[Ausschalten:]
            \item $i=\frac{U_0}{R}\cdot e^{-\frac{R}{L}t}$
            \item $u_L=-U_0\cdot e^{-\frac{R}{L}t}$
            \item[$\Rightarrow$] Strom nimmt ab, Spannung wird umgekehrt und nimmt dann (betragsmäßig) ab
        \end{tightitemize}
    \subsubsection{Filter}
        \begin{tightitemize}
            \item Hochpass: hohe Frequenzen werden übermittelt, langsame gedämpft
            \item Tiefpass: langsame Frequenzen werden übermittelt, hohe gedämpft
        \end{tightitemize}
\section{Halbleiterbauelemente}
\subsection{Diode}
    \begin{tightitemize}
        \item n-dotiert: Elektonen-Überschuss
        \item p-dotiert: Elektonenmangel
        \item Zusammenspiel von Drift- (el. Feld) und Diffusionsstrom (Konzentration)
        \item + an p, - an n $\rightarrow$ leitend
        \item - an p, + an n $\rightarrow$ sperrend bis Durchbruchspannung
        \item Schaltzeichen: ``Dreieck'' ist p-dotiert, Strich n-dotiert
    \end{tightitemize}
\subsection{Transistor}
    \begin{tightitemize}
        \item Basis, Collector und Emitter (Bipolar)
        \item Gate, Drain und Source + Bulk (FET)
        \item NPN: Pfeil nach außen, PNP: Pfeil zum Transistor
        \item FET: Sperrschicht FET: negative Spannung sperrt Kanal
        \item MOSFET: Isolierschicht FET
        \begin{tightitemize}
            \item n-Kanal (NMOS): p-Substrat, Kanal zwischen n-dotierten Einlagerungen, Pfeil zum Transistor
            \item p-Kanal (PMOS): n-Substrat, Kanal zwischen p-dotierten Einlagerungen, Pfeil nach außen
        \end{tightitemize}
    \end{tightitemize}
    \subsubsection{MOSFET}
        \begin{tightitemize}
            \item[Sperrbereich:] $U_{GS}\le U_{th}$ und $U_{DS}>0 \Rightarrow I_D\approx 0$
            \item[Widerstandbereich:] $U_{GS}>U_{th}$ und $0<U_{DS}<U_{GS}-U_{th}$ $\Rightarrow I_D=\beta_n\left[\left(U_{GS}-U_{th}\right)U_{DS}-\frac{U_{DS}^2}{2}\right]$
            \item[Sättigungsbereich:] $U_{GS}>U_{th}$ und $U_{GS}-U_{th}\le U_{DS}$ $\Rightarrow I_D=\frac{\beta_n}{2}\left(U_{GS}-U_{th}\right)^2$
        \end{tightitemize}
        \begin{tightitemize}
            \item nMOS bessere Eigenschaften, pMOS einfach herzustellen
            \item CMOS: Kombination, theoretisch kein Stromverbrauch (außer beim Umschalten)
        \end{tightitemize}
\section{\dots CMOS, ROM, PAL, PLA, \dots}
    kp, wie das abgefragt werden soll... und keine Ahnung, was man da lernen sollte - vgl. UB14
\section{CMOS-Produktion}
    siehe Folien, taugt auch nicht wirklich für ne Zusammenfassung
\end{multicols*}\end{document}
